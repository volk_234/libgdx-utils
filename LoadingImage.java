package com.lifeapp.shadowracers.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;

public class LoadingImage extends WidgetGroup {
    private ShapeRenderer shapeRenderer;
    private Vector2 center = new Vector2();
    private Vector2 top = new Vector2();

    private float angleStep = 5; //step of animation in degrees
    private float updatePeriod = 0.0001f; //update period
    private float progress; //current degrees

    private int directional = 1; //rotation direction
    private boolean loop = true; //should be looped

    private Action updateAction;


    /* usage

        LoadingImage loadingImage = new LoadingImage(shapeRenderer);
        loadingImage.setColor(Color.BLUE);
        loadingImage.setPosition(Gdx.graphics.getWidth() * 0.5f, Gdx.graphics.getHeight() * 0.5f);
        stage.add(loadingImage);
    */

    public LoadingImage(ShapeRenderer shapeRenderer) {
        this.shapeRenderer = shapeRenderer;

        updateAction = Actions.forever(Actions.delay(
                updatePeriod, Actions.run(this::update)
        ));
        addAction(updateAction);
        setSize(getPrefWidth(), getPrefHeight());
    }

    @Override
    public float getPrefHeight() {
        return 200;
    }

    @Override
    public float getPrefWidth() {
        return 200;
    }

    public void update() {
        this.progress += angleStep * directional;
        if ((progress > 360) || (progress < 0)) {
            if (loop) {
                directional *= -1;
            } else {
                removeAction(updateAction);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        center.x = getX(Align.center);
        center.y = getY(Align.center);
        top.x = getX(Align.center);
        top.y = getY(Align.top);

        Matrix4 shapeRendererMatrix = shapeRenderer.getTransformMatrix();
        Color shapeRendererColor = shapeRenderer.getColor();

        shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
        getColor().a = parentAlpha;
        shapeRenderer.setColor(getColor());

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        Vector2 drawPoint = top.cpy();
        for (float i = 0; i < progress; i += angleStep) {
            Vector2 drawPoint1 = drawPoint.cpy().sub(center).rotate(angleStep * directional).add(center);
            shapeRenderer.triangle(center.x, center.y, drawPoint.x, drawPoint.y, drawPoint1.x, drawPoint1.y);
            drawPoint = drawPoint1;
        }

        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
        shapeRenderer.setTransformMatrix(shapeRendererMatrix);
        shapeRenderer.setColor(shapeRendererColor);

        batch.begin();
    }
}
